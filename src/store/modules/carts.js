import {http} from '../../http/axios-config.js'

export default {
    state: {
        carts: []
    },
    mutations: {
        updateCarts(state, carts){
            state.carts = carts
        }
    },
    actions: {
        setCarts(ctx){
            return new Promise((resolve, reject) => {
                http.get(`/carts/${localStorage.getItem('id')}/`)
                .then(res => {
                    ctx.commit('updateCarts', res.data)
                    resolve(res)
                })
                .catch(err => reject(err))
            })
        },
    },
    getters: {
        carts(state){
            return state.carts
        }
    }
}