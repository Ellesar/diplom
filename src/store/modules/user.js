import {http} from '../../http/axios-config.js'

export default {
    state: {
        user: []
    },
    mutations: {
        updateUser(state, user){
            state.user = user
        }
    },
    actions: {
        coockie(ctx,crenditials){
            return new Promise((resolve, reject) => {
                http.patch(`/users/${localStorage.getItem('id')}/`, JSON.stringify(crenditials))
                .then(res => {
                    ctx.commit('updateUser', res.data)
                    resolve(res)
                })
                .catch(err => reject(err))
            })
        },
        async setUser(ctx){
            const user = await http.get('/users/' + localStorage.getItem('id') + '/')
            ctx.commit('updateUser', user.data)
        },
    },
    getters: {
        user(state){
            return state.user
        }
    }
}