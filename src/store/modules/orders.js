import {http} from '../../http/axios-config.js'

export default {
    state: {
        orders: []
    },
    mutations: {
        updateOrders(state, orders){
            state.orders = orders
        }
    },
    actions: {
        setOrders(ctx){
            return new Promise((resolve, reject) => {
                http.get(`/orders/${localStorage.getItem('id')}/`)
                .then(res => {
                    ctx.commit('updateOrders', res.data)
                    resolve(res)
                })
                .catch(err => reject(err))
            })
        },
    },
    getters: {
        orders(state){
            return state.orders
        }
    }
}