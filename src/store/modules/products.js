import {http} from '../../http/axios-config.js'

export default {
    state: {
        products: []
    },
    mutations: {
        updateProducts(state, products){
            state.products = products
        }
    },
    actions: {
        setProducts(ctx){
            return new Promise((resolve, reject) => {
                http.get('/products/')
                .then(res => {
                    ctx.commit('updateProducts', res.data)
                    resolve(res)
                })
                .catch(err => reject(err))
            })
        },
    },
    getters: {
        SetProducts(state){
            return state.products
        }
    }
}