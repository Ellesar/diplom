import {http} from '../../http/axios-config.js'

export default {
    state: {
        token: localStorage.getItem('authorization_token'),
        id: localStorage.getItem('id'),
    },
    mutations: {
        updateAuth(state, token, id){
            state.token = token;
            state.id = id
        },
        sign_out(state){
            state.token = null,
            state.id = null
        }
    },
    actions: {
        sign_in(ctx,crenditials){
            return new Promise((resolve, reject) => {
                http.post('/auth/signin/', JSON.stringify(crenditials))
                .then(res => {
                    localStorage.setItem('authorization_token', res.data.authorization_token)
                    localStorage.setItem('id', res.data.id)
                    ctx.commit('updateAuth', res.data.authorization_token, res.data.id)
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            })
        },
        sign_up(ctx,crenditials){
            return new Promise((resolve, reject) => {
                http.post('/auth/signup/', JSON.stringify(crenditials))
                .then(res => resolve(res))
                .catch(err => reject(err))
            })
        },
        sign_out(ctx){
            localStorage.clear();
            ctx.commit('sign_out')
        }
    },
    getters: {
        isLogged(state){
            return state.token
        }
    }
}