import Vue from 'vue'
import Vuex from 'vuex'
//modules
import auth from './modules/auth.js'
import user from './modules/user.js'
import products from './modules/products.js'
import orders from './modules/orders.js'
import carts from './modules/carts.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,user,products,orders,carts
  }
})
