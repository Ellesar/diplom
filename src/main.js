import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
//package
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css';
//filter
import currencyFilter from './filter/currency.js'

Vue.config.productionTip = false
Vue.use(VueToast)
Vue.filter('currency', currencyFilter)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
