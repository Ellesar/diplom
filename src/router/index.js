import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/sign_in",
    name: "Login",
    meta: {
      layout: "log",
      title: "Вход в систему",
    },
    component: () => import("../views/SignViews/Login"),
  },
  {
    path: "/sign_up",
    name: "Registration",
    meta: {
      layout: "log",
      title: "Регистрация",
    },
    component: () => import("../views/SignViews/Registration.vue"),
  },
  {
    path: "/catalog",
    name: "Main",
    meta: {
      layout: "main",
      title: "Каталог товаров",
    },
    component: () => import("../views/MainViews/Catalog.vue"),
  },
  {
    path: "/techmap",
    name: "Main",
    meta: {
      layout: "main",
      title: "Каталог товаров",
    },
    component: () => import("../views/MainViews/TechMap.vue"),
  },
  {
    path: "/",
    name: "Home",
    meta: {
      layout: "main",
      title: "Домашняя страница",
    },
    component: () => import("../views/MainViews/Home.vue"),
  },
  {
    path: "/office",
    name: "Office",
    meta: {
      layout: "main",
      title: "Личный кабинет",
    },
    component: () => import("../views/MainViews/Office.vue"),
  },
  {
    path: "/about",
    name: "About",
    meta: {
      layout: "main",
      title: "О нас",
    },
    component: () => import("../views/MainViews/About.vue"),
  },
  {
    path: "/contacts",
    name: "Contacts",
    meta: {
      layout: "main",
      title: "Контакты",
    },
    component: () => import("../views/MainViews/Contacts.vue"),
  },
  {
    path: "/price",
    name: "Price",
    meta: {
      layout: "price",
      title: "Корзина",
    },
    component: () => import("../views/PayViews/Price.vue"),
  },
  {
    path: "/payments",
    name: "Payments",
    meta: {
      layout: "pay",
      title: "Оплата",
    },
    component: () => import("../views/PayViews/Pay.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
